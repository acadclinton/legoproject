#include "LegoLayer.h"
using namespace std;
LegoLayer::LegoLayer(int w, int h, int** shape)
{
	width=w;
	height=h;
	board=new LegoBlock**[height];
	for(int i=0;i<height;i++)
	{
		board[i]=new LegoBlock*[w];
		for(int j=0;j<width;j++)
		{
			if(shape[i][j]==1)
			{
				board[i][j]=new LegoBlock(i,j);
				blocks.push_back(board[i][j]);
			}
			else
				board[i][j]=NULL;
		}
	}
	for(int i=0;i<height;i++)
		for(int j=0;j<width;j++)
			if(board[i][j]!=NULL)
			{
				for(int k=0;k<4;k++)
				{
					int tx=ax[k]+i;
					int ty=ay[k]+j;
					if(board[tx][ty]!=NULL)
					{
						board[i][j]->SetAdjacency(board[tx][ty]);
						board[tx][ty]->SetAdjacency(board[i][j]);
					}
				}
			}
			
}
LegoLayer::~LegoLayer()
{
	for(int i=0;i<blocks.size();i++)
	{
		delete blocks[i];
	}
	for(int i=0;i<height;i++)
	{
		delete board[i];	
	}
	delete board;
}
int LegoLayer::Merge(LegoBlock* b1,LegoBlock* b2)
{
	if(!(b1->isAdjacent(b2)))
	{
		cout<<"Not Adjacent"<<endl;
		return 0;
	}
	int mergetest=MergeRestriction(b1,b2);
	if(!mergetest)
	{
		cout<<"Fail Restriction"<<endl;
		return 0;
	}
	LegoBlock* result=b1->Merge(b2,mergetest);
	blocks.erase(find(blocks.begin(),blocks.end(),b2));
	vector<XY>* list=NULL;
	list=result->GetOccupying();
	int n=list->size();
	for(int i=0;i<n;i++)
	{
		int nx=(list->at(i)).x;
		int ny=(list->at(i)).y;
		board[nx][ny]=result;

	}
	
	for(int i=0;i<n;i++)
	{
		int nx=(list->at(i)).x;
		int ny=(list->at(i)).y;
		for(int j=0;j<4;j++)
		{
			int tx=nx+ax[j];
			int ty=ny+ay[j];
			if(board[tx][ty]!=NULL){
				result->SetAdjacency(board[tx][ty]);
				board[tx][ty]->SetAdjacency(result);	
			}
		}

	}

	return mergetest;
}

int LegoLayer::Split(LegoBlock* b1)
{
	vector<XY>* list=NULL;
	list=b1->GetOccupying();
	int n=list->size();
	for(int i=0;i<n;i++)
	{
		int nx=(list->at(i)).x;
		int ny=(list->at(i)).y;

		board[nx][ny]=new LegoBlock(nx,ny);
		blocks.push_back(board[nx][ny]);
		for(int j=0;j<4;j++)
		{
			int tx=nx+ax[j];
			int ty=ny+ay[j];
			if(board[tx][ty]!=NULL){
				board[tx][ty]->RemoveAdjacency(b1);	
			}
		}

	}
	for(int i=0;i<n;i++)
	{
		int nx=(list->at(i)).x;
		int ny=(list->at(i)).y;
		for(int j=0;j<4;j++)
		{
			int tx=nx+ax[j];
			int ty=ny+ay[j];
			if(board[tx][ty]!=NULL){
				board[nx][ny]->SetAdjacency(board[tx][ty]);
				board[tx][ty]->SetAdjacency(board[nx][ny]);	
			}
		}
	}
	blocks.erase(find(blocks.begin(),blocks.end(),b1));
	delete b1;
}
int LegoLayer::MergeRestriction(LegoBlock* &b1, LegoBlock* &b2)
{
	XY xy1=b1->GetRepPosition();
	XY xy2=b2->GetRepPosition();
	cout<<xy1.x<<","<<xy1.y<<" + "<<xy2.x<<","<<xy2.y<<endl;
	if(xy1.x==xy2.x)
	{
		if(xy1.y>xy2.y)
		{
			LegoBlock* t=b1;
			b1=b2;
			b2=t;
			xy1=b1->GetRepPosition();
			xy2=b2->GetRepPosition();	
		}
	}
	else if(xy1.y==xy2.y)
	{
		if(xy1.x>xy2.x)
		{
			LegoBlock* t=b1;
			b1=b2;
			b2=t;
			xy1=b1->GetRepPosition();
			xy2=b2->GetRepPosition();	
		}
	}
	else return 0;
	int type1=b1->GetType();
	int type2=b2->GetType();
	if(xy1.x==xy2.x)
	{
		int h1=type1/10;
		int w1=type1%10;
		int h2=type2/10;
		int w2=type2%10;
		int nh=h1;
		if(nh>3) return 0;
		int nw=w1+w2;
		if((nw%2==0&& nw<=8) || nw==3 )
			return nh*10+nw;
		else return 0;
	}
	if(xy1.y==xy2.y)
	{
		int h1=type1/10;
		int w1=type1%10;
		int h2=type2/10;
		int w2=type2%10;
		int nw=w1;
		if(nw>3) return 0;
		int nh=h1+h2;
		if((nh%2==0&&nh<=8) || nh==3)
			return nh*10+nw;
		else return 0;
	}


	
	

	return 0;

	
}
void LegoLayer::Print()
{
	map<LegoBlock*,int> blocklist;
	int cnt=0;
	for(int i=0;i<height;i++)
	{
		for(int j=0;j<width;j++)
		{

			if(board[i][j]!=NULL)
			if(blocklist.find(board[i][j])==blocklist.end())
			{
				cnt++;
				blocklist[board[i][j]]=cnt;
			}
		}
	}
	for(int i=0;i<height;i++)
	{
		for(int j=0;j<width;j++)
		{
			cout<<blocklist[board[i][j]]<<"\t";
		}
		cout<<endl;
	}

}
LegoBlock* LegoLayer::GetBlock(int index)
{
	return blocks[index];
}
LegoBlock* LegoLayer::GetBlock(int x,int y)
{
	return board[x][y];

}
void LegoLayer::SetBelowLayer(LegoLayer* b)
{
	below=b;
}

int LegoLayer::Score()
{
	return 0;
}
