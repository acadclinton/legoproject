CC = g++
OBJS = LegoBlock.o LegoLayer.o LegoBody.o testmain.o
TARGET = test

.SUFFIXES : .cpp .o

all :  $(TARGET)

$(TARGET): $(OBJS)
	$(CC) -o $@ $(OBJS)


clean :
	rm -f $(OBJS) $(TARGET)
