#include "LegoBlock.h"
using namespace std;
LegoBlock::LegoBlock(int i,int j)
{
	n=1;
	type=11;
	occupying=new vector<XY>();
	repxy.x=i;
	repxy.y=j;
	occupying->push_back(repxy);
	adjacency=new vector<LegoBlock*>();
}
LegoBlock::~LegoBlock()
{
	delete occupying;
	delete adjacency;
}
void LegoBlock::PrintSpec()
{
	cout<<"Type:"<<type<<"\txy:"<<repxy.x<<","<<repxy.y<<endl;
}

LegoBlock* LegoBlock::Merge(LegoBlock* target,int mergetype)
{
	type=mergetype;
	vector<XY>* tl=target->GetOccupying();
	int n=tl->size();
	for(int i=0;i<n;i++)
	{
		XY t=tl->at(i);
		occupying->push_back(t);
	}
	delete target;
	delete adjacency;
	adjacency=new vector<LegoBlock*>();	
	
	
	return this;
}

vector<XY>* LegoBlock::GetOccupying()
{
	return occupying;
}
vector<LegoBlock*>* LegoBlock::GetAdjacentList()
{
	return adjacency;
}

bool LegoBlock::isAdjacent(LegoBlock* target)
{
	if(find(adjacency->begin(), adjacency->end(),target)!=adjacency->end())
	{
		return true;
	}
	return false;
	
}
void LegoBlock::SetAdjacency(LegoBlock* target)
{
	if(target==this)return;
	if(find(adjacency->begin(), adjacency->end(),target)!=adjacency->end())
	{
		return;
	}
	else
	{
		adjacency->push_back(target);
	}
}
void LegoBlock::RemoveAdjacency(LegoBlock* target)
{
	if(find(adjacency->begin(),adjacency->end(),target)==adjacency->end()) return;	
	adjacency->erase(find(adjacency->begin(),adjacency->end(),target));
	
}
XY LegoBlock::GetRepPosition()
{
	return repxy;
}
int LegoBlock::GetType()
{
	return type;
}
