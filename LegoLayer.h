#ifndef LEGOLAYER
#define LEGOLAYER
#include "LegoBlock.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
class LegoLayer
{

private:
	LegoBlock*** board;
	
	int width;
	int height;
	int MergeRestriction(LegoBlock* &b1, LegoBlock* &b2); //b1와 b2가 병합 가능한지 확인, 병합가능하다면 그 결과 블록이 어떤 type인지 리턴, 아니면 0 리턴.
	std::vector<LegoBlock*> blocks;
	LegoLayer* below;
	
	


public:
	
	LegoLayer(int w,int h,int** shape); //w: 가로 h: 세로 shape: 배치
	~LegoLayer();
	void Print();// 레고 배치도 출력
	void SetBelowLayer(LegoLayer* b);
	int Merge(LegoBlock* b1, LegoBlock* b2);//b1과 b2를 합침. 성공할경우 결과물의 type(양수)를 리턴, 실패할경우 0 리턴
	LegoBlock* GetBlock(int index);//blocks 리스트에서 인덱스로 블록 선택.
	LegoBlock* GetBlock(int x,int y);//board에서 좌표로 블록 선택.
	int Split(LegoBlock* b1);//b1을 분할.
	int GetWidth();
	int GetHeight();
	int Score();
};
#endif
