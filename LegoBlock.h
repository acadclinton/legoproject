#ifndef LEGOBLOCK
#define LEGOBLOCK
#include<vector>
#include<iostream>
#include<algorithm>
#include "Shared.h"
class LegoBlock
{
private:
	std::vector<XY>* occupying;
	std::vector<LegoBlock*>* adjacency;
	int type;
	XY repxy;
	int n;
public:
	LegoBlock(int i,int j);
	~LegoBlock();

	void PrintSpec();//이 블록의 스펙 출력
	LegoBlock* Merge(LegoBlock* target,int type);//이블록과 target을 병합(가능조건 따지지 않고 병합 후 type이 된다고 가정.
	std::vector<XY>* GetOccupying();//이 블록이 차지하는 좌표목록 
	std::vector<LegoBlock*>* GetAdjacentList();//이 블록과 인접한 블록목록
	void SetAdjacency(LegoBlock* target);//인접 설정
	void RemoveAdjacency(LegoBlock* target);//인접 설정 제거
	bool isAdjacent(LegoBlock* target);//인접한지 확인.
	int GetType();//타입 출력
	XY GetRepPosition();//대표좌표(좌측상단)
	
	
};
#endif
