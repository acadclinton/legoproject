#ifndef LEGOBODY
#define LEGOBODY
#include "LegoLayer.h"
#include<iostream>

class LegoBody
{
private:
	LegoLayer** layers;
	int width;
	int height;
	int depth;
	
public:
	LegoBody(int w,int h, int d, int*** shape);
	~LegoBody();
	LegoLayer* GetLayer(int p);
	

};


#endif

