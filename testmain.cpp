#include<iostream>
#include"LegoLayer.h"
#include"LegoBlock.h"
#include"LegoBody.h"
using namespace std;

const int testboard[2][9][10]={{
{0,0,0,0,0,0,0,0,0,0},
{0,0,1,1,1,1,1,0,0,0},
{0,1,1,1,1,1,1,1,1,0},
{0,1,1,1,0,0,1,1,1,0},
{0,0,1,1,0,0,1,1,0,0},
{0,0,1,1,1,1,1,1,1,0},
{0,1,1,1,1,1,1,1,1,0},
{0,0,0,0,1,1,1,1,0,0},
{0,0,0,0,0,0,0,0,0,0}
},{
{0,0,0,0,0,0,0,0,0,0},
{0,0,1,1,1,1,1,0,0,0},
{0,1,1,1,1,1,1,1,1,0},
{0,1,1,1,0,0,1,1,1,0},
{0,0,1,1,0,0,1,1,0,0},
{0,0,1,1,1,1,1,1,1,0},
{0,1,1,1,1,1,1,1,1,0},
{0,0,0,0,1,1,1,1,0,0},
{0,0,0,0,0,0,0,0,0,0}}};



int main()
{
	int d=2,m=9,n=10;
	for(int k=0;k<d;k++){
		for(int i=0;i<m;i++){
			for(int j=0;j<n;j++){
				cout<<testboard[k][i][j];
				
			}
			cout<<endl;
		}
		cout<<endl;
	}
	cout<<endl;
	int*** tt;
	tt=new int**[d];
	for(int i=0;i<d;i++)
	{
		tt[i]=new int*[m];
		for(int j=0;j<m;j++)
		{
			tt[i][j]=new int[n];
			for(int k=0;k<n;k++)
			{
				tt[i][j][k]=testboard[i][j][k];
			}
		}
	}
	LegoBody body(2,10,9,tt);


	LegoLayer* layer=body.GetLayer(0);
	layer->Print();
	cout<<endl;
	LegoBlock* b1;
	LegoBlock* b2;

	b1=layer->GetBlock(1,2);
	cout<<"Merge Target Block"<<endl;
	b1->PrintSpec();
	b2=layer->GetBlock(2,2);
	cout<<"Merge Target Block"<<endl;
	b2->PrintSpec();
	if(layer->Merge(b1,b2)==0) cout<<"Fail"<<endl;
	layer->Print();
	cout<<endl;

	cout<<"Merge Target Block"<<endl;
	b1=layer->GetBlock(1,3);
	b1->PrintSpec();
	cout<<"Merge Target Block"<<endl;
	b2=layer->GetBlock(2,3);
	b2->PrintSpec();
	if(layer->Merge(b1,b2)==0) cout<<"Fail"<<endl;
	layer->Print();
	cout<<endl;

	cout<<"Merge Target Block"<<endl;
	b1=layer->GetBlock(1,4);
	b1->PrintSpec();
	cout<<"Merge Target Block"<<endl;
	b2=layer->GetBlock(1,5);
	b2->PrintSpec();
	if(layer->Merge(b1,b2)==0) cout<<"Fail"<<endl;
	layer->Print();
	cout<<endl;

	cout<<"Merge Target Block"<<endl;
	b1=layer->GetBlock(2,4);
	b1->PrintSpec();
	cout<<"Merge Target Block"<<endl;
	b2=layer->GetBlock(2,5);
	b2->PrintSpec();
	if(layer->Merge(b1,b2)==0) cout<<"Fail"<<endl;
	layer->Print();
	cout<<endl;



	cout<<"Merge Target Block"<<endl;
	b1=layer->GetBlock(1,2);
	b1->PrintSpec();
	cout<<"Merge Target Block"<<endl;
	b2=layer->GetBlock(1,3);
	b2->PrintSpec();
	if(layer->Merge(b1,b2)==0) cout<<"Fail"<<endl;
	layer->Print();
	cout<<endl;

	cout<<"Merge Target Block"<<endl;
	b1=layer->GetBlock(1,4);
	b1->PrintSpec();
	cout<<"Merge Target Block"<<endl;
	b2=layer->GetBlock(2,4);
	b2->PrintSpec();
	if(layer->Merge(b1,b2)==0) cout<<"Fail"<<endl;
	layer->Print();
	cout<<endl;


	cout<<"Merge Target Block"<<endl;
	b1=layer->GetBlock(1,2);
	b1->PrintSpec();
	cout<<"Merge Target Block"<<endl;
	b2=layer->GetBlock(1,4);
	b2->PrintSpec();
	if(layer->Merge(b1,b2)==0) cout<<"Fail"<<endl;
	layer->Print();
	cout<<endl;
	
	cout<<"Split Block"<<endl;
	b1=layer->GetBlock(1,2);
	b1->PrintSpec();
	layer->Split(b1);
	layer->Print();
	cout<<endl;


	b1=layer->GetBlock(1,2);
	cout<<"Merge Target Block"<<endl;
	b1->PrintSpec();
	b2=layer->GetBlock(2,2);
	cout<<"Merge Target Block"<<endl;
	b2->PrintSpec();
	if(layer->Merge(b1,b2)==0) cout<<"Fail"<<endl;
	layer->Print();
	cout<<endl;

	cout<<"Merge Target Block"<<endl;
	b1=layer->GetBlock(1,3);
	b1->PrintSpec();
	cout<<"Merge Target Block"<<endl;
	b2=layer->GetBlock(2,3);
	b2->PrintSpec();
	if(layer->Merge(b1,b2)==0) cout<<"Fail"<<endl;
	layer->Print();
	cout<<endl;

	cout<<"Merge Target Block"<<endl;
	b1=layer->GetBlock(1,2);
	b1->PrintSpec();
	cout<<"Merge Target Block"<<endl;
	b2=layer->GetBlock(1,3);
	b2->PrintSpec();
	if(layer->Merge(b1,b2)==0) cout<<"Fail"<<endl;
	layer->Print();
	cout<<endl;
	
	cout<<"Split Block"<<endl;
	b1=layer->GetBlock(1,2);
	b1->PrintSpec();
	layer->Split(b1);
	layer->Print();
	cout<<endl;



	





	
	return 0;
}
