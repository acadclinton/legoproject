#include "LegoBody.h"


LegoBody::LegoBody(int d, int w, int h, int*** shape)
{
	width=w;
	height=h;
	depth=d;
	layers=new LegoLayer*[d];
	
	for(int i=0;i<depth;i++)
	{
		layers[i]=new LegoLayer(w,h,shape[i]);
	}
}
LegoBody::~LegoBody()
{
	for(int i=0;i<depth;i++)
		delete layers[i];
	delete layers;
		
}
LegoLayer* LegoBody::GetLayer(int p)
{
	return layers[p];
}
